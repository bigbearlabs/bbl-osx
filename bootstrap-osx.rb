#!/usr/bin/env ruby

# NOTE superceded by dependencies.yaml

# ASSERT brew installed.

`brew tap caskroom/cask`
`brew tap caskroom/versions`

%w(
	rbenv
	ruby-build
	npm
	terminal-notifier
	redis
	ack

).map do |brew_pkg|
	`brew install #{brew_pkg}`
end

# HCI
%w(
	karabiner
	keyboard-maestro
	quicksilver
	totalterminal
	bettertouchtool
	1password
	hyperswitch
) + 

# system
%w(
	little-snitch
	istat-menus
) +

# development
%w(
	sublime-text3
	google-chrome-canary
	sourcetree
	dash
	atom
	iterm2-beta
) +
# util
%w(
	skype
	evernote
	dropbox
	google-drive
	iphone-configuration-utility
	wireshark
).map do |cask_pkg|
	`brew cask install #{cask_pkg}`
end

# ruby / gem deps
`rbenv install 2.1.1`
`gem install bundler`
`gem install sass`

# npm deps
`npm install -g coffee-script`
`npm install -g brunch`

class OSXEnvironment
    def self.library_files(app_names, exclusions = [])
        paths =
            app_names.map do |app|
                # resource paths
                [
                    "~/Library/Preferences/*#{app}*",
                    "~/Library/Application Support/*#{app}*/**/*",
                    "~/Library/Containers/*#{app}*/**/*"
                ]
              end .map do |paths|
                # paths that exist
                paths.map do |path|
                    Dir.glob "#{path}".sub( "~", ENV["HOME"]), File::FNM_CASEFOLD
                end
            end

        file_set = paths.flatten

        file_set_with_exclusions = file_set.select do |path]
          matches = exclusions.select { |e| path =~ %r{#{e}} }
          matches.empty?
        end
    end
end

def use_case( app_names = %w[
        Karabiner
        Terminal
        LittleSnitch
        Quicksilver
        Keyboard Maestro
        istatmenus
        totalterminal
        Xcode
        Dash
        SourceTree
        rubymine
    ]
)

    exclusions = [
        "Dash/.*?DocSets",
        "Dash/Temp",
        "Application Support/SourceTree/SourceTree"
    ]

    OSXEnvironment.library_files app_names, exclusions
end


# EVAL
  # p use_case ['Dash']

  # use_case(['Dash']).count


# it0
# # usage sketches

# track :bbl_osx, OSXEnvironment.library_files  # to start tracking the files from an osx host

# rsync :bbl_osx, ENV["HOME"], dry:true, relative:true  # to install tracked files onto an osx host



# it2
# consider returning a fileset object of some kind.

### sketch of a design that hides logic realising execution patterns that require e.g. command visibility, early failure, clean syntax.


class String
	def run(dry_run = false)
		# if $DEBUG
			puts "running cmd: #{self}"
		 # end

		unless dry_run 
			system self
		end
	end
end


#== test case

# would be nice if UI facilitates interactive extraction of variables like these.
app = "diagnostics"
work_dir = "~/dev/src/#{app}"

%(
	cd #{work_dir}
	time PATH=~/.gem/ruby/2.0.0/bin:"$PATH" ipa build --verbose

	rsync -av #{app}.ipa ~/dev/src/mm2PackagingFactory/data/apps/#{app}/#{app}.ipa 
	(cd  ~/dev/src/mm2PackagingFactory && rake app:{package,deploy}[#{app},ch-etit])
)
# .run

# this was copied / pasted from shell history.
# would be nice if UI lets us start with a shell, then facilitates the grabbing of reusable chunks liks this.
